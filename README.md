# VLC-Sync

**NOTE**: As a work in progress, VLC-Sync is undergoing active development and is not yet usable.

VLC-Sync is a contemporary adaptation of [OMXPlayer-Sync](https://github.com/turingmachine/omxplayer-sync), using VLC as a replacement for the [deprecated OMXPlayer](https://github.com/popcornmix/omxplayer/commit/1f1d0ccd65d3a1caa86dc79d2863a8f067c8e3f8). This implementation facilitates the synchronization of multiple VLC instances across a network, following a conductor/follower model.
